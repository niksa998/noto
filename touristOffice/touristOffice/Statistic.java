package touristOffice;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.SwingConstants;

public class Statistic extends JFrame {

	public Statistic() {

		setBounds(0, 0, 1100, 580);
		layout();

		JFrame lblStatistik = new JFrame("Statistik exportieren");
		lblStatistik.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblStatistik.setBounds(170, 100, 200, 20);
		lblStatistik.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().add(lblStatistik);

		JButton btnExport = new JButton("Export to PDF");
		btnExport.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				MessageFormat ueberschrift = new MessageFormat("Star Statistics");
				MessageFormat fusszeile = new MessageFormat("Page {0, number, integer}");
				try {
					getTableSS().print(JTable.PrintMode.FIT_WIDTH, ueberschrift, fusszeile);

				} catch (java.awt.print.PrinterException e1) {
					JOptionPane.showMessageDialog(null, "While printing an error occured!");
					e1.printStackTrace();
				}
			}
		});

		btnExport.setBounds(828, 100, 124, 25);
		getContentPane().add(btnExport);

		loadTableSS();
	}

	protected JTable getTableSS() {
		return null;
	}

	public void loadTableSS() {
		Connection con = null;
		PreparedStatement pst = null;

		try {
			String statement = "select  Category as 'Stars', count(id) as 'Hotels per Category',Sum(noRooms) as 'Sum of Rooms', Sum(noBeds) as 'Sum of Beds' from Hotel group by Category order by Category desc";
			pst = con.prepareStatement(statement);
			ResultSet rs = pst.executeQuery();
			JTable table = new JTable(buildTableModel(rs));

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static DefaultTableModel buildTableModel(ResultSet rs) throws SQLException {

		java.sql.ResultSetMetaData metaData = rs.getMetaData();

		// names of columns
		Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 1; column <= columnCount; column++) {
			columnNames.add(metaData.getColumnName(column));
		}

		// data of the table
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {
			Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
				vector.add(rs.getObject(columnIndex));
			}
			data.add(vector);
		}

		return new DefaultTableModel(data, columnNames);

	}
}
