package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AlleBetriebe {

	private JFrame frmStatistikDerBelegung;
	private JTable table;
	private JTable tableAll;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlleBetriebe window = new AlleBetriebe();
					window.frmStatistikDerBelegung.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws SQLException
	 */
	public AlleBetriebe() throws SQLException {
		initialize();
		showOcc();
		showSum();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmStatistikDerBelegung = new JFrame();
		frmStatistikDerBelegung.setFont(new Font("Dubai", Font.BOLD, 17));
		frmStatistikDerBelegung.setTitle("Statistik der Belegung");
		frmStatistikDerBelegung.setBounds(100, 100, 845, 447);
		frmStatistikDerBelegung.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmStatistikDerBelegung.getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 37, 809, 224);
		frmStatistikDerBelegung.getContentPane().add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 305, 809, 39);
		frmStatistikDerBelegung.getContentPane().add(scrollPane_1);

		tableAll = new JTable();
		scrollPane_1.setViewportView(tableAll);

		JLabel lblSum = new JLabel("Insgesamt");
		lblSum.setFont(new Font("Dubai", Font.BOLD, 15));
		lblSum.setHorizontalAlignment(SwingConstants.CENTER);
		lblSum.setBounds(322, 272, 167, 33);
		frmStatistikDerBelegung.getContentPane().add(lblSum);
		
		JButton StatistikBelegungClose = new JButton("Close");
		StatistikBelegungClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmStatistikDerBelegung.dispose();			}
		});
		StatistikBelegungClose.setBounds(730, 374, 89, 23);
		frmStatistikDerBelegung.getContentPane().add(StatistikBelegungClose);
	}

	

	public void showOcc() throws SQLException {

		Connection con = DatabaseConnection.getConnection();

		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("Kategorie");
		model.addColumn("Betriebe");
		model.addColumn("Zimmer");
		model.addColumn("Betten");
		try {
			String query = "SELECT Kategorie, COUNT(h.HotelID) AS Betriebe, SUM(h.ZimmerNr) AS Zimmer, SUM(h.BettenNr) AS Betten FROM HOTELS H GROUP BY Kategorie";

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				model.addRow(new Object[] { rs.getString("Kategorie"), rs.getInt("Betriebe"), rs.getInt("Zimmer"),
						rs.getString("Betten") });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Problem beim Laden!");
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);

	}

	public void showSum() throws SQLException {
		
		Connection con = DatabaseConnection.getConnection();

		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("Betriebe");
		model.addColumn("Zimmer");
		model.addColumn("Betten");
		try {
			String query = "Select count(h.HotelID) AS Betriebe ,SUM(h.ZimmerNr) AS Zimmer, SUM(h.BettenNr) AS Betten From Hotels h ";

			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("Betriebe"), rs.getInt("Zimmer"), rs.getString("Betten") });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Problem beim Laden!");
		}

		tableAll.setModel(model);
		tableAll.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		tableAll.getColumnModel().getColumn(0);
		tableAll.getColumnModel().getColumn(1);
		tableAll.getColumnModel().getColumn(2);
	}
}
