package touristOffice;

public class Hotel {

	String Kategorie, Name, Besitzer, Kontakt, Adresse, Stadt;
	int HotelID, Telefon, ZimmerNr, BettenNr, PLZ;

	public Hotel(int HotelID, String Kategorie, String Name, String Besitzer, String Kontakt, String Adresse,
			String Stadt, int PLZ, int Telefon, int ZimmerNr, int BettenNr) {
		this.HotelID = HotelID;
		this.Kategorie = Kategorie;
		this.Name = Name;
		this.Besitzer = Besitzer;
		this.Kontakt = Kontakt;
		this.Adresse = Adresse;
		this.Stadt = Stadt;
		this.PLZ = PLZ;
		this.Telefon = Telefon;
		this.ZimmerNr = ZimmerNr;
		this.BettenNr = BettenNr;
	}

	public String getKategorie() {
		return Kategorie;
	}

	public void setKategorie(String kategorie) {
		Kategorie = kategorie;
	}

	public int getHotelID() {
		return HotelID;
	}

	public void setHotelID(int HotelID) {
		this.HotelID = HotelID;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getBesitzer() {
		return Besitzer;
	}

	public void setBesitzer(String besitzer) {
		Besitzer = besitzer;
	}

	public String getKontakt() {
		return Kontakt;
	}

	public void setKontakt(String kontakt) {
		Kontakt = kontakt;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public String getStadt() {
		return Stadt;
	}

	public void setStadt(String stadt) {
		Stadt = stadt;
	}

	public int getTelefon() {
		return Telefon;
	}

	public void setTelefon(int telefon) {
		Telefon = telefon;
	}

	public int getZimmerNr() {
		return ZimmerNr;
	}

	public void setZimmerNr(int zimmerNr) {
		ZimmerNr = zimmerNr;
	}

	public int getBettenNr() {
		return BettenNr;
	}

	public void setBettenNr(int bettenNr) {
		BettenNr = bettenNr;
	}

	public int getPLZ() {
		return PLZ;
	}

	public void setPLZ(int pLZ) {
		PLZ = pLZ;
	}

}
