package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HotelListe {

	private JFrame frmListeAllerHotels;
	private JTable table;
	private JScrollPane scrollPane;
	private JTextField IdText;
	private JTextField nameField;
	private JTextField textDeleteID;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HotelListe window = new HotelListe();
					window.hotelsAnzeigen();
					window.frmListeAllerHotels.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void hotelsAnzeigen() throws SQLException {

		Connection con = DatabaseConnection.getConnection();

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");

		try {
			String query = "SELECT * FROM Hotels";
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			

			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Kategorie"), rs.getString("Name"),
						rs.getString("Besitzer"), rs.getString("Kontakt"), rs.getString("Adresse"),
						 rs.getString("Stadt"), rs.getInt("PLZ"), rs.getInt("Telefon"),
						rs.getInt("ZimmerNr"), rs.getInt("BettenNr") });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Problem beim Laden!");
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		table.getColumnModel().getColumn(6);
		table.getColumnModel().getColumn(7);
		table.getColumnModel().getColumn(8);
		table.getColumnModel().getColumn(9);
		table.getColumnModel().getColumn(10);

	}
	
	public void hotelAnzeigen() throws SQLException {

		
		Connection con = null;
		PreparedStatement queryStatement = null;

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");

		
		try {
			if(!IdText.getText().isEmpty()) {
			String query = "SELECT * FROM Hotels WHERE HotelID = ?";
			con = DatabaseConnection.getConnection();
			queryStatement = con.prepareStatement(query);
			queryStatement.setString(1, IdText.getText());
			}
			else if(!nameField.getText().isEmpty() && IdText.getText().isEmpty()) {
				
				String query = "SELECT * FROM Hotels WHERE Name LIKE ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1,nameField.getText());
			}
			else {
				String query = "SELECT * FROM Hotels";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
			}
			//TODO Für die Benutzerinnen und Benutzer erklären, wie man etwas löschen kann, und dass die ID -Suchleiste den Vorzug im Vergleich zu Nmaeleise hat
			
			ResultSet rs = queryStatement.executeQuery();
			
			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Kategorie"), rs.getString("Name"),
						rs.getString("Besitzer"), rs.getString("Kontakt"), rs.getString("Adresse"),
						 rs.getString("Stadt"), rs.getInt("PLZ"), rs.getInt("Telefon"),
						rs.getInt("ZimmerNr"), rs.getInt("BettenNr") });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Problem beim Laden!");
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		table.getColumnModel().getColumn(6);
		table.getColumnModel().getColumn(7);
		table.getColumnModel().getColumn(8);
		table.getColumnModel().getColumn(9);
		table.getColumnModel().getColumn(10);

	}

	public void hotelLöschen() throws Exception {
		
		Connection con = DatabaseConnection.getConnection();
		PreparedStatement statement = null;
		try {		
			String query = "DELETE FROM Hotels WHERE HotelID = ? ";
			statement = con.prepareStatement(query);
			
			statement.setString(1, textDeleteID.getText());
			ResultSet rs = statement.executeQuery();
		}
		catch(Exception e) {
		}
	}
	
	
	public HotelListe() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmListeAllerHotels = new JFrame();
		frmListeAllerHotels.setTitle("Liste aller Hotels");
		frmListeAllerHotels.setBounds(100, 100, 1226, 464);
		frmListeAllerHotels.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmListeAllerHotels.getContentPane().setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 97, 1200, 326);
		frmListeAllerHotels.getContentPane().add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
			}
		
		
		});
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);
		scrollPane.setViewportView(table);

		JButton btAnzeigen = new JButton("Hotel anzeigen");
		btAnzeigen.setFont(new Font("Dubai", Font.PLAIN, 15));
		btAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					hotelAnzeigen();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		btAnzeigen.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
			}
		});
		btAnzeigen.setBounds(322, 35, 218, 35);
		frmListeAllerHotels.getContentPane().add(btAnzeigen);
		
		JButton btDelete = new JButton("Hotel Löschen");
		btDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				
				if(!textDeleteID.getText().isEmpty()) {
				int ask = JOptionPane.showConfirmDialog(btDelete, "Sind Sie sicher dass Sie das Hotel mit der ID Nummer: " + textDeleteID.getText() + " löschen möchten?", "Hotel löschen", JOptionPane.YES_NO_OPTION); 
				
				if (ask == JOptionPane.YES_OPTION) {
					try {
						hotelLöschen();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (ask == JOptionPane.NO_OPTION) {
						
				} else {

				}
			}else {
				JOptionPane.showMessageDialog(null, "Sie haben keine ID-Nummer eingetragen!");
			}
		}
			
		protected boolean isSelectionEditable(JTable table) {
			if(table.getRowSelectionAllowed()) {
				int columnCount = table.getColumnCount();
				int[] selectedRows = table.getSelectedRows();
				for(int selectedRow : selectedRows) {
					for(int column = 0; column < columnCount; column++) {
						if(table.isCellEditable(selectedRow, column))
							return true;
					}
				}
			}
			if(table.getColumnSelectionAllowed() ) {
				int rowCount = table.getRowCount();
				int[] selectedColumns = table.getSelectedColumns();
				for(int selectedColumnn : selectedColumns) {
					for(int row = 0; row < rowCount; row++) {
						if(table.isCellEditable(row, selectedColumnn))
							return true;
					}
				}
			}
			return false;
		}
		


			
			
		});
		btDelete.setFont(new Font("Dubai", Font.PLAIN, 15));
		btDelete.setBounds(604, 12, 260, 44);
		frmListeAllerHotels.getContentPane().add(btDelete);
		
		JLabel HotelID = new JLabel("Hotel ID:");
		HotelID.setFont(new Font("Dialog", Font.BOLD, 14));
		HotelID.setBounds(23, 20, 91, 25);
		frmListeAllerHotels.getContentPane().add(HotelID);
		
		IdText = new JTextField();
		IdText.setBounds(132, 16, 178, 35);
		frmListeAllerHotels.getContentPane().add(IdText);
		IdText.setColumns(10);
		
		nameField = new JTextField();
		nameField.setColumns(10);
		nameField.setBounds(132, 52, 178, 35);
		frmListeAllerHotels.getContentPane().add(nameField);
		
		JLabel lblHotelname = new JLabel("Hotelname:");
		lblHotelname.setFont(new Font("Dialog", Font.BOLD, 14));
		lblHotelname.setBounds(23, 57, 91, 25);
		frmListeAllerHotels.getContentPane().add(lblHotelname);
		
		JLabel HotelIDdelete = new JLabel("Hotel ID:");
		HotelIDdelete.setFont(new Font("Dialog", Font.BOLD, 14));
		HotelIDdelete.setBounds(614, 58, 84, 35);
		frmListeAllerHotels.getContentPane().add(HotelIDdelete);
		
		textDeleteID = new JTextField();
		textDeleteID.setColumns(10);
		textDeleteID.setBounds(696, 59, 168, 35);
		frmListeAllerHotels.getContentPane().add(textDeleteID);
		
		btnNewButton = new JButton("Änderungen speichern");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(1016, 23, 168, 33);
		frmListeAllerHotels.getContentPane().add(btnNewButton);
	}
}
