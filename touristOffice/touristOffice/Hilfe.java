package touristOffice;


import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class Hilfe extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Hilfe frame = new Hilfe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Hilfe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("PDF Handbuch ");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
					File file = new File("C:\\Users\\Niksa Marinkovic\\Desktop\\Niksa\\Faks\\4. Semester\\Project Management Cycle\\User Manual.pdf");
					//check if the file exists, and if so open it
					
					if(file.exists()) {
						if(Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(file);
						}else {
							JOptionPane.showMessageDialog(Hilfe.this, "Not Supported");
						}
					}else {
						JOptionPane.showMessageDialog(Hilfe.this, "File Not Found!");
					}
					
				}catch(Exception pdf) {
					
				}
				
			}
		});
		btnNewButton.setBounds(272, 227, 152, 23);
		contentPane.add(btnNewButton);
	}
}
