package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterForm {

	private JFrame frmRegisterform;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterForm window = new RegisterForm();
					window.frmRegisterform.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegisterform = new JFrame();
		frmRegisterform.setTitle("Registerform");
		frmRegisterform.setBounds(100, 100, 404, 246);
		frmRegisterform.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmRegisterform.getContentPane().setLayout(null);
		
		JButton btnUser = new JButton("AppUser");
		btnUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BenutzerRegisterForm.main(null);			}
		});
		btnUser.setBounds(276, 6, 88, 197);
		btnUser.setFont(new Font("Dubai", Font.BOLD, 14));
		btnUser.setBackground(new Color(153, 204, 255));
		frmRegisterform.getContentPane().add(btnUser);
		
		JButton btnBesitzer = new JButton("Besitzer");
		btnBesitzer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BesitzerRegisterForm.main(null);	
			}
		});
		btnBesitzer.setBounds(35, 6, 85, 197);
		btnBesitzer.setFont(new Font("Dubai", Font.BOLD, 14));
		btnBesitzer.setBackground(new Color(0, 204, 102));
		frmRegisterform.getContentPane().add(btnBesitzer);
		
		JLabel lblNewLabel = new JLabel("Registrieren Sie sich als");
		lblNewLabel.setBounds(124, 30, 153, 148);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Dubai", Font.BOLD, 12));
		frmRegisterform.getContentPane().add(lblNewLabel);
	}

}
