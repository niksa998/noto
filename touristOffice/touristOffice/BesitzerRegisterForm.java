package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BesitzerRegisterForm<ItemT> extends JFrame {

	private JFrame frame;
	private JTextField nameText;
	private JTextField mailText;
	private JTextField passwortText;
	private JTextField passwortText2;
	private JButton btnNewButton;
	private JButton btnAbbrechen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BesitzerRegisterForm window = new BesitzerRegisterForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throwsSQLException
	 */
	public BesitzerRegisterForm() throws SQLException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 461, 446);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		nameText = new JTextField();
		nameText.setBounds(183, 113, 141, 19);
		frame.getContentPane().add(nameText);
		nameText.setColumns(10);

		mailText = new JTextField();
		mailText.setColumns(10);
		mailText.setBounds(183, 158, 141, 19);
		frame.getContentPane().add(mailText);

		passwortText = new JPasswordField();
		passwortText.setColumns(10);
		passwortText.setBounds(183, 205, 141, 19);
		frame.getContentPane().add(passwortText);

		passwortText2 = new JPasswordField();
		passwortText2.setColumns(10);
		passwortText2.setBounds(183, 252, 141, 19);
		frame.getContentPane().add(passwortText2);

		JLabel lblPasswortWiederholen = new JLabel("Passwort wiederholen :");
		lblPasswortWiederholen.setFont(new Font("Dubai", Font.PLAIN, 14));
		lblPasswortWiederholen.setHorizontalAlignment(SwingConstants.CENTER);
		lblPasswortWiederholen.setBounds(32, 252, 141, 16);
		frame.getContentPane().add(lblPasswortWiederholen);

		JLabel lblNewLabel_3_1 = new JLabel("Passwort :");
		lblNewLabel_3_1.setFont(new Font("Dubai", Font.PLAIN, 14));
		lblNewLabel_3_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3_1.setBounds(89, 208, 84, 16);
		frame.getContentPane().add(lblNewLabel_3_1);

		JLabel lblNewLabel_3_2 = new JLabel("E-Mail Adresse :");
		lblNewLabel_3_2.setFont(new Font("Dubai", Font.PLAIN, 14));
		lblNewLabel_3_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3_2.setBounds(73, 158, 100, 16);
		frame.getContentPane().add(lblNewLabel_3_2);

		JLabel lblNewLabel_3_3 = new JLabel("Besitzername :");
		lblNewLabel_3_3.setFont(new Font("Dubai", Font.PLAIN, 14));
		lblNewLabel_3_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3_3.setBounds(73, 116, 100, 16);
		frame.getContentPane().add(lblNewLabel_3_3);

		JLabel lblNewLabel = new JLabel("Besitzer anlegen");
		lblNewLabel.setFont(new Font("Dubai", Font.BOLD, 17));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(183, 24, 141, 44);
		frame.getContentPane().add(lblNewLabel);

		btnNewButton = new JButton("Registrieren");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					register();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
			}
		});
		btnNewButton.setBounds(248, 325, 158, 31);
		frame.getContentPane().add(btnNewButton);

		btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnAbbrechen.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		btnAbbrechen.setBounds(66, 325, 158, 31);
		frame.getContentPane().add(btnAbbrechen);
	}

	public void register() throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;
		PreparedStatement queryStatement2 = null;

		try {
			con = DatabaseConnection.getConnection();
			String statement = "INSERT INTO BesitzerLogin values (?,?,?)";
			String statement2 = "SELECT*FROM BesitzerLogin WHERE Benutzername = (?)";
			queryStatement = con.prepareStatement(statement);
			queryStatement2 = con.prepareStatement(statement2);
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "Problem mit der Verbindung!");
		}

		queryStatement2.setString(1, nameText.getText());

		ResultSet result = queryStatement2.executeQuery();
		if(result.next()) {
			JOptionPane.showMessageDialog(null, "Der Benutzername existiert schon!");
			nameText.setText("");
			passwortText.setText("");
			passwortText2.setText("");
			mailText.setText("");
		}

		queryStatement.setString(1, nameText.getText());
		queryStatement.setString(2, passwortText.getText());
		queryStatement.setString(3, mailText.getText());

		if (passwortText.getText().equals(passwortText2.getText())) {
			if (passwortText.getText().equals(nameText.getText())) {
				JOptionPane.showMessageDialog(null, "Benutzername darf nicht identisch wie das Passwort sein!");
			} else {

				if (result.next()) {
					JOptionPane.showMessageDialog(null, "Der Benutzername existiert schon!");
					nameText.setText("");
					passwortText.setText("");
					passwortText2.setText("");
					mailText.setText("");
				} else {
					int countOfRows = 0;

					countOfRows = queryStatement.executeUpdate();
					boolean rs = queryStatement.getMoreResults();

					JOptionPane.showMessageDialog(null, "Erfolgreich registriert!");

					if (countOfRows == 1) {
						nameText.setText("");
						passwortText.setText("");
						passwortText2.setText("");
						mailText.setText("");
					}
				}
			}
		}else {
			JOptionPane.showMessageDialog(null, "Passwoerter muessen uebereinander stimmen!");
		}
	}
}
