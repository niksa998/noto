package touristOffice;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import javax.swing.JPasswordField;
import javax.swing.JTextField;


import java.awt.Font;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class LoginForm extends JFrame {

	public static void main(String[] args) {
		new LoginForm().setVisible(true);

	}
	
	private JButton bt_submit = new JButton("Anmelden");
	private JTextField tf_user = new JTextField(10);
	private JPasswordField tf_pwd = new JPasswordField(10);
	private JLabel lb_user = new JLabel("Benutzername:");
	private JLabel lb_pwd = new JLabel("Passwort:");
	private final JButton btnRegistrieren = new JButton("Neue Benutzer Registrieren");

	public LoginForm() {

		setTitle("Login");
		setSize(470, 277);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		
		lb_user.setBounds(62, 49, 90, 20);
		getContentPane().add(lb_user);
		tf_user.setBounds(149, 46, 150, 27);
		getContentPane().add(tf_user);
		lb_pwd.setBounds(84, 86, 60, 27);

		getContentPane().add(lb_pwd);
		tf_pwd.setBounds(149, 86, 150, 27);
		getContentPane().add(tf_pwd);
		bt_submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showHome();
			}
		});
		bt_submit.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
			}
		});
		bt_submit.setFont(new Font("Dubai", Font.BOLD, 12));
		bt_submit.setBounds(172, 124, 111, 38);

		getContentPane().add(bt_submit);
		tf_pwd.setEchoChar('*');
		btnRegistrieren.setFont(new Font("Dubai", Font.BOLD, 12));
		btnRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BenutzerRegisterForm.main(null);
			}
		});
		btnRegistrieren.setBounds(10, 186, 175, 41);
		
		getContentPane().add(btnRegistrieren);
		
		JButton btnNeueBesitzerRegistrieren = new JButton("Neue Besitzer Registrieren");
		btnNeueBesitzerRegistrieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BesitzerRegisterForm.main(null);
			}
		});
		btnNeueBesitzerRegistrieren.setFont(new Font("Dubai", Font.BOLD, 12));
		btnNeueBesitzerRegistrieren.setBounds(269, 186, 175, 41);
		getContentPane().add(btnNeueBesitzerRegistrieren);

//		 pack();

	}

	public void showHome() {
		Connection con = null;
		try {
			con = DatabaseConnection.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String query = "SELECT Benutzername,Passwort FROM LoginDaten WHERE Benutzername=? AND Passwort=?";

		try {
			PreparedStatement st = null;

			con = DatabaseConnection.getConnection();
			st = con.prepareStatement(query);

			st.setString(1, tf_user.getText());
			st.setString(2, tf_pwd.getText());

			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				JOptionPane.showMessageDialog(null, "Erfolgreiche Anmeldung!");
				dispose();
				HomeWindow hw = new HomeWindow();
				hw.main(null);
			} else {
				JOptionPane.showMessageDialog(null, "Inkorrekte Daten eingegeben!");
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}
}